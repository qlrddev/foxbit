import unittest
import datetime
import time
from foxbit.frame import MessageFrame


class MessageFrameTest(unittest.TestCase):

    def test_000_default_message_frame(self):
        frame = MessageFrame()
        self.assertEqual(frame.messageType, 0)
        self.assertEqual(frame.sequenceNumber, 0)
        self.assertEqual(frame.endpoint, 'LogOut')
        self.assertDictEqual(frame.payload, {})

    def test_001_invalid_type_messageType(self):
        with self.assertRaises(TypeError):
            MessageFrame(messageType={})

    def test_002_invalid_value_messageType(self):
        with self.assertRaises(ValueError):
            MessageFrame(messageType='noneofthese')

    def test_003_invalid_value_messageType(self):
        with self.assertRaises(ValueError):
            MessageFrame(messageType='noneofthese')

    def test_004_invalid_type_sequenceNumber_str(self):
        with self.assertRaises(TypeError):
            MessageFrame(sequenceNumber='0')

    def test_005_invalid_type_sequenceNumber_float(self):
        with self.assertRaises(TypeError):
            MessageFrame(sequenceNumber=0.1)

    def test_006_invalid_type_endpoint(self):
        with self.assertRaises(TypeError):
            MessageFrame(endpoint=0)

    def test_007_invalid_value_endpoint(self):
        with self.assertRaises(ValueError):
            MessageFrame(endpoint='GetNotExists')

    def test_008_invalid_type_payload_int(self):
        with self.assertRaises(TypeError):
            MessageFrame(payload=0)

    def test_009_invalid_type_payload_float(self):
        with self.assertRaises(TypeError):
            MessageFrame(payload=0.1)

    def test_010_invalid_type_payload_str(self):
        with self.assertRaises(TypeError):
            MessageFrame(payload='{}')

    def test_011_valid_type_and_value_messageType(self):
        count = 0
        _list = (
            'request',
            'reply',
            'subscribe',
            'event',
            'unsubscribe',
            'error'
        )
        for r in _list:
            frame = MessageFrame(messageType=r)
            self.assertEqual(frame.messageType, count)
            count += 1

    def test_012_valid_type_and_value_sequenceNumber(self):
        count = 0
        for r in range(10):
            frame = MessageFrame(sequenceNumber=r)
            self.assertEqual(frame.sequenceNumber, count)
            count += 1

    def test_013_valid_type_and_value_endpoint_public(self):
        count = 0
        for e in MessageFrame.PUBLIC_ENDPOINTS:
            frame = MessageFrame(endpoint=e)
            el = MessageFrame.PUBLIC_ENDPOINTS[count]
            self.assertEqual(frame.endpoint, el)
            count += 1

    def test_014_valid_type_and_value_endpoint_private(self):
        count = 0
        for e in MessageFrame.PRIVATE_ENDPOINTS:
            frame = MessageFrame(endpoint=e)
            el = MessageFrame.PRIVATE_ENDPOINTS[count]
            self.assertEqual(frame.endpoint, el)
            count += 1

    def test_015_valid_type_and_value_payload_empty(self):
        frame = MessageFrame(payload={})
        self.assertDictEqual(frame.payload, {})

    def test_016_valid_type_and_value_payload_non_empty(self):
        frame = MessageFrame(payload={'OMSId': 1})
        self.assertDictEqual(frame.payload, {'OMSId': 1})

    def test_017_toJSON(self):
        frame = MessageFrame()
        msg = "{\"m\": 0, \"i\": 0, \"n\": \"LogOut\", \"o\": \"{}\"}"
        self.assertEqual(frame.toJSON(), msg)

    def test_018_fromJSON(self):
        msg = "{\"m\": 0, \"i\": 0, \"n\": \"LogOut\", \"o\": \"{}\"}"
        frame = MessageFrame.fromJSON(msg)
        self.assertEqual(frame.toJSON(), msg)

    def test_019_invalid_type_buildValidPosixTimeString_None(self):
        with self.assertRaises(TypeError):
            MessageFrame.buildValidPosixTimeString()

    def test_020_invalid_type_buildValidPosixTimeString_int(self):
        with self.assertRaises(TypeError):
            MessageFrame.buildValidPosixTimeString(dateAndtime=0)

    def test_021_invalid_type_buildValidPosixTimeString_str(self):
        with self.assertRaises(TypeError):
            MessageFrame.buildValidPosixTimeString(dateAndtime='none')

    def test_022_valid_buildValidPosixTimeString(self):
        now = datetime.datetime.now()
        __now__ = MessageFrame.buildValidPosixTimeString(now)
        self.assertIsInstance(__now__, str)
        __time__ = time.strptime(__now__, "%Y-%m-%dT%H:%M:%S")
        timestamp = time.mktime(__time__)
        __now__ = datetime.datetime.fromtimestamp(timestamp)
        self.assertEqual(now.year, __now__.year)
        self.assertEqual(now.month, __now__.month)
        self.assertEqual(now.day, __now__.day)
        self.assertEqual(now.hour, __now__.hour)
        self.assertEqual(now.minute, __now__.minute)
        self.assertEqual(__now__.second, 0)
