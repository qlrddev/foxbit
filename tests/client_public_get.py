import unittest
import datetime
from foxbit.client import PublicClient
from foxbit.frame import MessageFrame


class PublicClientGetTest(unittest.TestCase):

    def test_000_get_instrument(self):
        def on_open(ws):
            foxbit.getInstrument(
                OMSId=1,
                InstrumentId=1
            )

        def on_message(ws, msg):
            self.assertIsNotNone(msg)
            self.assertIsInstance(msg, str)
            parsed = MessageFrame.fromJSON(msg)
            # print(parsed)
            self.assertIsInstance(parsed, MessageFrame)
            self.assertEqual(parsed.endpoint, 'GetInstrument')
            self.assertIsInstance(parsed.payload, dict)
            foxbit.close()

        foxbit = PublicClient(on_open=on_open, on_message=on_message)
        foxbit.run_forever()

    def test_001_get_instruments(self):
        def on_open(ws):
            foxbit.getInstruments(
                OMSId=1,
            )

        def on_message(ws, msg):
            self.assertIsNotNone(msg)
            self.assertIsInstance(msg, str)
            parsed = MessageFrame.fromJSON(msg)
            # print(parsed)
            self.assertEqual(parsed.messageType, 0)
            self.assertEqual(parsed.sequenceNumber, 0)
            self.assertEqual(parsed.endpoint, 'GetInstruments')
            self.assertIsInstance(parsed.payload, list)
            self.assertGreater(len(parsed.payload), 1)
            foxbit.close()

        foxbit = PublicClient(on_open=on_open, on_message=on_message)
        foxbit.run_forever()

    def test_002_get_products(self):
        def on_open(ws):
            foxbit.getProducts(
                OMSId=1
            )

        def on_message(ws, msg):
            self.assertIsNotNone(msg)
            self.assertIsInstance(msg, str)
            parsed = MessageFrame.fromJSON(msg)
            # print(parsed)
            self.assertEqual(parsed.messageType, 0)
            self.assertEqual(parsed.sequenceNumber, 0)
            self.assertEqual(parsed.endpoint, 'GetProducts')
            self.assertIsInstance(parsed.payload, list)
            foxbit.close()

        foxbit = PublicClient(on_open=on_open, on_message=on_message)
        foxbit.run_forever()

    def test_003_get_L2_snapshot(self):
        def on_open(ws):
            foxbit.getL2Snapshot(
                OMSId=1,
                InstrumentId=1,
                Depth=1
            )

        def on_message(ws, msg):
            self.assertIsNotNone(msg)
            self.assertIsInstance(msg, str)
            parsed = MessageFrame.fromJSON(msg)
            # print(parsed)
            self.assertEqual(parsed.messageType, 0)
            self.assertEqual(parsed.sequenceNumber, 0)
            self.assertEqual(parsed.endpoint, 'GetL2Snapshot')
            self.assertIsInstance(parsed.payload, list)
            for __list__ in parsed.payload:
                self.assertIsInstance(__list__, list)
            foxbit.close()

        foxbit = PublicClient(on_open=on_open, on_message=on_message)
        foxbit.run_forever()

    def test_004_get_ticker_history(self):
        def on_open(ws):
            __now__ = datetime.datetime.now()
            __from__ = datetime.datetime(
                __now__.year,
                __now__.month,
                __now__.day - 1,
                __now__.hour,
                __now__.minute,
                __now__.second
            )
            foxbit.getTickerHistory(
                InstrumentId=1,
                Interval=60,
                ToDate=__now__,
                FromDate=__from__
            )

        def on_message(ws, msg):
            self.assertIsNotNone(msg)
            self.assertIsInstance(msg, str)
            parsed = MessageFrame.fromJSON(msg)
            # print(parsed)
            self.assertEqual(parsed.messageType, 0)
            self.assertEqual(parsed.sequenceNumber, 0)
            self.assertEqual(parsed.endpoint, 'GetTickerHistory')
            self.assertIsInstance(parsed.payload, list)
            for o in parsed.payload:
                self.assertIsInstance(o, list)
            foxbit.close()

        foxbit = PublicClient(on_open=on_open, on_message=on_message)
        foxbit.run_forever()
