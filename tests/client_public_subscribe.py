import unittest
from foxbit.client import PublicClient
from foxbit.frame import MessageFrame


class ClientSubscribeTest(unittest.TestCase):

    def test_000_subscribe_and_unsubscribe_ticker(self):
        def on_open(ws):
            foxbit.subscribeTicker(
                OMSId=1,
                InstrumentId=1,
                Interval=60,
                IncludeLastCount=100
            )

        def on_subscribe(frame):
            self.assertIsInstance(frame.payload, list)
            self.assertGreater(len(frame.payload), 1)

        def on_update(frame):
            self.assertIsInstance(frame.payload, list)
            self.assertEqual(len(frame.payload), 1)
            foxbit.unsubscribeTicker(
                OMSId=1,
                InstrumentId=1
            )

        def on_unsubscribe(frame):
            self.assertIsInstance(frame.payload, dict)
            self.assertTrue(frame.payload['result'])
            self.assertIsNone(frame.payload['errormsg'])
            self.assertEqual(frame.payload['errorcode'], 0)
            self.assertIsNone(frame.payload['detail'])
            foxbit.close()

        def on_message(ws, msg):
            parsed = MessageFrame.fromJSON(msg)
            # print(parsed)
            foxbit.onSubscription(
                subscription='SubscribeTicker',
                frame=parsed,
                callback=on_subscribe
            )
            foxbit.onSubscription(
                subscription='TickerDataUpdateEvent',
                frame=parsed,
                callback=on_update
            )
            foxbit.onSubscription(
                subscription='UnsubscribeTicker',
                frame=parsed,
                callback=on_unsubscribe
            )

        def on_error(ws, err):
            print(err)

        foxbit = PublicClient(
            on_open=on_open,
            on_message=on_message,
            on_error=on_error
        )
        foxbit.run_forever()

    def test_001_subscribe_and_unsubscribe_level1_InstrumentId(self):
        def on_open(ws):
            foxbit.subscribeLevel1(
                OMSId=1,
                InstrumentId=1
            )

        def on_subscribe(frame):
            self.assertIsInstance(frame.payload, dict)

        def on_update(frame):
            self.assertIsInstance(frame.payload, dict)
            foxbit.unsubscribeLevel1(
                OMSId=1,
                InstrumentId=1
            )

        def on_unsubscribe(frame):
            self.assertIsInstance(frame.payload, dict)
            self.assertTrue(frame.payload['result'])
            self.assertIsNone(frame.payload['errormsg'])
            self.assertEqual(frame.payload['errorcode'], 0)
            self.assertIsNone(frame.payload['detail'])
            foxbit.close()

        def on_message(ws, msg):
            parsed = MessageFrame.fromJSON(msg)
            # print(parsed)
            foxbit.onSubscription(
                subscription='SubscribeLevel1',
                frame=parsed,
                callback=on_subscribe
            )
            foxbit.onSubscription(
                subscription='Level1UpdateEvent',
                frame=parsed,
                callback=on_update
            )
            foxbit.onSubscription(
                subscription='UnsubscribeLevel1',
                frame=parsed,
                callback=on_unsubscribe
            )

        def on_error(ws, err):
            print(err)

        foxbit = PublicClient(
            on_open=on_open,
            on_message=on_message,
            on_error=on_error
        )

        foxbit.run_forever()

    def test_002_subscribe_and_unsubscribe_level2_InstrumentId(self):
        def on_open(ws):
            foxbit.subscribeLevel2(
                OMSId=1,
                InstrumentId=1,
                Depth=10
            )

        def on_subscribe(frame):
            self.assertIsInstance(frame.payload, list)

        def on_update(frame):
            self.assertIsInstance(frame.payload, list)
            foxbit.unsubscribeLevel2(
                OMSId=1,
                InstrumentId=1
            )

        def on_unsubscribe(frame):
            self.assertIsInstance(frame.payload, dict)
            self.assertTrue(frame.payload['result'])
            self.assertIsNone(frame.payload['errormsg'])
            self.assertEqual(frame.payload['errorcode'], 0)
            self.assertIsNone(frame.payload['detail'])
            foxbit.close()

        def on_message(ws, msg):
            parsed = MessageFrame.fromJSON(msg)
            # print(parsed)
            foxbit.onSubscription(
                subscription='SubscribeLevel2',
                frame=parsed,
                callback=on_subscribe
            )
            foxbit.onSubscription(
                subscription='Level2UpdateEvent',
                frame=parsed,
                callback=on_update
            )
            foxbit.onSubscription(
                subscription='UnsubscribeLevel2',
                frame=parsed,
                callback=on_unsubscribe
            )

        def on_error(ws, err):
            print(err)

        foxbit = PublicClient(
            on_open=on_open,
            on_message=on_message,
            on_error=on_error
        )

        foxbit.run_forever()
