import unittest
from foxbit.client import Client


class ClientOpenCloseTest(unittest.TestCase):

    def test_000_on_open(self):
        def on_open(ws):
            self.assertTrue(foxbit.keep_running)
            foxbit.close()
            self.assertFalse(foxbit.keep_running)

        foxbit = Client(on_open=on_open)
        foxbit.run_forever()

    def test_001_on_close(self):

        def on_open(ws):
            self.assertTrue(foxbit.keep_running)
            foxbit.close()

        def on_close(ws):
            self.assertFalse(foxbit.keep_running)

        foxbit = Client(on_open=on_open, on_close=on_close)
        foxbit.run_forever()
