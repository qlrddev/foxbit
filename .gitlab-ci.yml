image: python:latest

stages:
  - build
  - test
  - staging
  - deploy
  - build_production
  - production

.install_script: &install_pip
  - pip install virtualenv tox tox-wheel

.activate_virtualenv_script: &activate_virtualenv
  - python -m virtualenv venv && source venv/bin/activate

.deactivate_virtualenv_script: &deactivate_virtualenv
  - deactivate
  - rm -rf ./venv

.exec_inline_foxbit: &foxbit_inline
  - python -c "from foxbit import client; print(client); print(client.Client)"
  - python -c "from foxbit import frame; print(frame); print(frame.MessageFrame)"
  - python -m pip uninstall -y foxbit

.upload_twine_foxbit: &twine_upload
  - python -m twine upload --repository-url $TWINE_REPO_URL $TOX_DIR/dist/*.whl

.build_stage: &build_script
  - *install_pip
  - tox --wheel --wheel-dirty

.test_stage: &test_script
  - *install_pip
  - *activate_virtualenv
  - python -m pip install --find-links=$TOX_DIR/dist foxbit
  - *foxbit_inline
  - *deactivate_virtualenv

.staging_stage: &staging_script
    - *install_pip
    - *activate_virtualenv
    - python -m pip install twine
    - *twine_upload
    - python -m pip install --index-url https://$TWINE_USERNAME:$TWINE_PASSWORD@$(cut --complement -c1-8 <<< $TWINE_REPO_URL)/simple foxbit
    - *foxbit_inline
    - *deactivate_virtualenv

.deploy_stage: &deploy_script
    - *install_pip
    - *activate_virtualenv
    - python -m pip install twine
    - *twine_upload
    - python -m pip install --index-url https://test.pypi.org/simple/ foxbit
    - *foxbit_inline
    - *deactivate_virtualenv

.build_production_stage: &build_production_script
  - pip install virtualenv wheel
  - *activate_virtualenv
  - python -m pip download --no-deps --index-url https://test.pypi.org/simple/ $PROGRAM
  - PACKAGE=$(ls -la $PROGRAM* | awk '{ print $9 }')
  - wheel unpack $PACKAGE
  - rm $PACKAGE
  - WHEEL_FOLDER=$(ls -la | awk '{ print $9 }' | grep $PROGRAM)
  - mkdir $PROGRAM-$VERSION
  - mkdir $PROGRAM-$VERSION/$PROGRAM
  - mkdir $PROGRAM-$VERSION/$PROGRAM-$VERSION.dist-info
  - cp $WHEEL_FOLDER/$WHEEL_FOLDER.dist-info/** $PROGRAM-$VERSION/$PROGRAM-$VERSION.dist-info/
  - cp -r $WHEEL_FOLDER/$PROGRAM $PROGRAM-$VERSION
  - CURRENT_VERSION=`cat $WHEEL_FOLDER/$WHEEL_FOLDER.dist-info/METADATA | grep ^Version | cut -d ' ' -f2`
  - echo $CURRENT_VERSION
  - sed -i 's/'$CURRENT_VERSION'$/'$VERSION'/g' $PROGRAM-$VERSION/$PROGRAM-$VERSION.dist-info/METADATA
  - rm $PROGRAM-$VERSION/$PROGRAM-$VERSION.dist-info/RECORD
  - wheel pack $PROGRAM-$VERSION
  - rm -rf $PROGRAM-$VERSION
  - rm -rf $WHEEL_FOLDER
  - PACKAGE=$(ls -la $PROGRAM*.whl | awk '{ print $9 }')
  - python -m pip install $PACKAGE
  - python -m pip list 
  - *foxbit_inline
  - *deactivate_virtualenv
  
variables:
  TOX_DIR: $CI_BUILDS_DIR/$GITLAB_USER_LOGIN/foxbit/.tox

# Build wheel distribuition file
build:
  stage: build
  only:
    - development
  artifacts:
    expire_in: 1 days
    paths:
      - $TOX_DIR/dist
  variables:
    CI_NAME: gitlab
    CI_BUILD_NUMBER: $CI_JOB_ID
    CI_BUILD_URL: $CI_JOB_URL
    CI_BRANCH: $CI_COMMIT_BRANCH
    COVERALLS_REPO_TOKEN: $COVERALLS_TOKEN
    COVERAGE_REPORT_FILE: $CI_BUILDS_DIR/$GITLAB_USER_LOGIN/foxbit/coverage.json
  script:
    - pip install tox tox-wheel coveralls
    - tox --wheel --wheel-dirty
    - coveralls --submit=$COVERAGE_REPORT_FILE
  
# Test the wheel distribuition file 
# in a local virtual environment
test:
  stage: test
  only:
    - development
  script:
    - pip install virtualenv
    - python -m virtualenv venv
    - source venv/bin/activate
    - python -m pip install --find-links=$TOX_DIR/dist foxbit
    - python -c "from foxbit import client; print(client); print(client.Client)"
    - python -c "from foxbit import frame; print(frame); print(frame.MessageFrame)"
    - python -m pip uninstall -y foxbit
    - deactivate
    - rm -rf ./venv

# Upload wheel distribution file
# to gitlab pypi index
staging:
  stage: staging
  only:
    - development
  variables:
    TWINE_PASSWORD: $CI_JOB_TOKEN
    TWINE_USERNAME: gitlab-ci-token
    TWINE_REPO_URL: $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/pypi
  script:
    - *staging_script

# Upload wheel distribution file
# to testpypi index
deploy:
  stage: deploy
  only:
    - development
  variables:
    TWINE_PASSWORD: $TESTPYPI_TOKEN
    TWINE_USERNAME: __token__
    TWINE_REPO_URL: https://test.pypi.org/legacy/
  script:
    - *deploy_script

# Download .whl file from testpypi index
# rename the version to more simple format
# test it (again) and save as artifact
build_production:
  stage: build_production
  only:
    - main
  variables:
    PROGRAM: foxbit
    VERSION: 0.0.12
  artifacts:
    expire_in: 1 days
    paths:
      - $CI_BUILDS_DIR/$GITLAB_USER_LOGIN/foxbit/$PROGRAM-$VERSION*.whl
  script:
    - *build_production_script

production:
  stage: production
  only:
    - main
  variables:
    PROGRAM: foxbit
    VERSION: 0.0.12
    TWINE_PASSWORD: $PYPI_TOKEN
    TWINE_USERNAME: __token__
  script:
    - pip install twine
    - python -m twine upload $CI_BUILDS_DIR/$GITLAB_USER_LOGIN/foxbit/$PROGRAM-$VERSION*.whl