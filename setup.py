import os
import math
import datetime
from setuptools import setup

__NAME__ = "foxbit"
__VERSION__ = "0.0.12"

with open("README.md", "r") as fh:
    long_description = fh.read()

if os.getenv('CI_COMMIT_BRANCH'):
    branch = os.getenv('CI_COMMIT_BRANCH')
    if (branch != 'main'):
        __time__ = datetime.datetime.now().timestamp()
        _time = math.floor(__time__)
        version = "{}{}".format(__VERSION__, _time)
    else:
        version = __VERSION__
else:
    version = __VERSION__

setup(
    name=__NAME__,
    version=version,
    description='A python client for foxbit exchange API',
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="Apache License Version 2.0",
    author='qlrd',
    author_email='qlrddev@gmail.com',
    maintainer='qlrd',
    maintainer_email='qlrddev@gmail.com',
    url='https://gitlab.com/qlrddev/foxbit/',
    keywords=[
        'websocket',
        'API',
        'exchange',
        'foxbit'
    ],
    packages=[
        'foxbit.client',
        'foxbit.frame'
    ],
    package_dir={
        "": "src",
    },
    install_requires=[
        'websocket-client>=1.2.3'
    ],
    extras_require={
        "test": [
            'tox-wheel>=0.7.0',
            'pytest>=6.2.5',
            'pytest-cov>=3.0.0',
            'flake8>=4.0.1',
            'coverage>=6.3.0'
        ]
    },
    classifiers=[
        'Development Status :: 1 - Planning',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Intended Audience :: Developers',
        'Natural Language :: Portuguese (Brazilian)',
        'Programming Language :: Python :: 3.10'
    ]
)
