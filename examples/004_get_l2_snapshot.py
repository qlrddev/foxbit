import os.path
import sys
sys.path.append(
    os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..")
    )
)
from foxbit.client import Client
from foxbit.frame import MessageFrame


def on_open(ws):
    """Assim que a conexão for aberta, envie uma requisição"""
    print("Connected to {}".format(client.url))
    frame = MessageFrame(
        endpoint='GetL2Snapshot',
        payload={'OMSId': 1, 'InstrumentId': 1, 'Depth': 100}
    )
    client.send(frame.toJSON())


def on_close(ws, status, msg):
    """Assim que a conexão for fechada, crie uma mensagem"""
    print("Disconnected")
    print("status {}".format(status))
    print("cause: {}".format(msg))


def on_message(ws, msg):
    """"Assim que a mensagem for recebida,
    transforme-a em um `MessageFrame`, imprima e feche a conexão"""
    print("Message received:")
    parsed = MessageFrame.fromJSON(msg)
    print(parsed)
    client.close()


def on_error(ws, err):
    print("Error: {}".format(err))


client = Client(
    on_open=on_open,
    on_close=on_close,
    on_message=on_message,
    on_error=on_error
)

client.run_forever()
