import os.path
import sys
sys.path.append(
    os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..")
    )
)
from foxbit.client import Client
from foxbit.frame import MessageFrame


def on_open(ws):
    print("Connected to {}".format(client.url))
    frame = MessageFrame(
        endpoint='SubscribeLevel1',
        payload={
            "OMSId": 1,
            "InstrumentId": 1
        }
    )
    client.send(frame.toJSON())


def on_close(ws):
    """Assim que a conexão for fechada, crie uma mensagem"""
    print("Disconnected")


def on_subscribe(frame):
    if(frame.endpoint == 'SubscribeLevel1'):
        print("received")


def on_update(frame):
    if(frame.sequenceNumber == 10 and frame.endpoint == 'Level1UpdateEvent'):
        newframe = MessageFrame(
            endpoint='UnsubscribeLevel1',
            payload={
                "OMSId": 1,
                "InstrumentId": 1
            }
        )
        client.send(newframe.toJSON())


def on_unsubscribe(frame):
    if(frame.endpoint == 'UnsubscribeLevel1'):
        client.close()


def on_message(ws, msg):
    """"Assim que a mensagem for recebida,
    transforme-a em um `dict`, imprima e feche a conexão"""
    frame = MessageFrame.fromJSON(msg)
    on_subscribe(frame)
    on_update(frame)
    on_unsubscribe(frame)
    print(frame)


def on_error(ws, err):
    print(err)


client = Client(
    on_open=on_open,
    on_close=on_close,
    on_message=on_message,
    on_error=on_error
)

client.run_forever()
