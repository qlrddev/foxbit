import os.path
import sys
sys.path.append(
    os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..")
    )
)
from foxbit.client import Client


def on_open(ws):
    """Assim que a conexão for aberta, envie uma requisição"""
    print("Connected to {}".format(client.url))
    client.close()


def on_close(ws, status, msg):
    """Assim que a conexão for fechada, crie uma mensagem"""
    print("Disconnected")
    print("status {}".format(status))
    print("cause: {}".format(msg))


def on_error(ws, err):
    print("Error: {}".format(err))


client = Client(
    on_open=on_open,
    on_close=on_close,
    on_error=on_error
)


client.run_forever()
