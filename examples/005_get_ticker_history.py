import os.path
import sys
sys.path.append(
    os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..")
    )
)
from foxbit.client import Client
from foxbit.frame import MessageFrame
import datetime


def on_open(ws):
    """Assim que a conexão for aberta, envie uma requisição"""
    print("Connected to {}".format(client.url))
    __now__ = datetime.datetime.now()
    __from__ = datetime.datetime(
        __now__.year,
        __now__.month,
        __now__.day - 1,
        __now__.hour,
        __now__.minute,
        __now__.second
    )
    frame = MessageFrame(
        endpoint='GetTickerHistory',
        payload={
            'InstrumentId': 1,
            'Interval': 60,
            'FromDate': MessageFrame.buildValidPosixTimeString(__from__),
            'ToDate': MessageFrame.buildValidPosixTimeString(__now__)
        }
    )
    client.send(frame.toJSON())


def on_close(ws, status, msg):
    """Assim que a conexão for fechada, crie uma mensagem"""
    print("Disconnected")
    print("status {}".format(status))
    print("cause: {}".format(msg))


def on_message(ws, msg):
    """"Assim que a mensagem for recebida,
    transforme-a em um `MessageFrame`, imprima e feche a conexão"""
    print("Message received:")
    parsed = MessageFrame.fromJSON(msg)
    print(parsed)
    client.close()


def on_error(ws, err):
    print("Error: {}".format(err))


client = Client(
    on_open=on_open,
    on_close=on_close,
    on_message=on_message,
    on_error=on_error
)

client.run_forever()
